'use strict'

const _ = require('lodash')
const moment = require('moment')

/**
 * 將 JSON 格式化成一行文字
 * @param {Object} obj JSON Object
 * @returns {String} 傳回字串
 */
const jsonInline = (obj) => require('util').inspect(obj, {
  breakLength: Infinity
})

// function about datetime

const TzTw = 480

/**
 * 判斷是否為正確的 Period 格式, 支援原(y)年(M)月(d)日(w)星期
 *
 * @param {String} s 字串
 *
 * @returns {Boolean} 是否為正確的 Period 格式
 */
const PeriodRE = /^\s*(\d+)\s*((?:months|weeks|days|years)|[Mwdy])\s*$/
const validPeriod = s => PeriodRE.test(s)

/**
 * 傳回(台灣時間)今年開始的日期
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const thisYear = () => moment.utc().utcOffset(TzTw)
  .set('month', 0)
  .set('date', 1)
  .format('YYYY-MM-DD')

/**
 * 傳回(台灣時間)本月開始的日期
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const thisMonth = () => moment.utc().utcOffset(TzTw)
  .set('date', 1)
  .format('YYYY-MM-DD')

/**
 * 傳回(台灣時間)本星期開始的日期
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const thisWeek = () => moment.utc().utcOffset(TzTw)
  .day(0)
  .format('YYYY-MM-DD')

/**
 * 傳回(台灣時間)當天開始的日期, 內定傳回今天
 *
 * @param {Number} unixTime linux 時間的毫秒數
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const thisDate = (unixTime) => (unixTime ? moment.utc(unixTime) : moment.utc()).utcOffset(TzTw)
  .format('YYYY-MM-DD')

/**
 * 傳回某日開始往前移動 n 個時間單位後零點的 linux 時間的毫秒數
 * date 為字串時, 代表是台灣時間
 * date 為 null 時, 內定為本日
 * perid 內定為 0d (不移動時間單位)
 *
 * e.q.  '3d' 等於三天前
 *
 * @param {String} perid 單位時間量
 * @param {String|Number|Date} date 日期或 linux 時間的毫秒數
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const atBefore = (perid, date) => {
  perid = perid || '0d'
  const [_input, nNum, sUnit] = PeriodRE.exec(perid)

  let time = moment.utc(date)
  time = (time.isValid() ? time : moment.utc())

  if (_.isString(date)) time.subtract(TzTw, 'minutes')
  if (_input == null) return thisDate(time)
  if (nNum === '0') {
    if (sUnit === 'M' || sUnit === 'months') {
      return time.utcOffset(TzTw).set('date', 1).format('YYYY-MM-DD')
    }
    if (sUnit === 'w' || sUnit === 'weeks') {
      return time.utcOffset(TzTw).day(0).format('YYYY-MM-DD')
    }
    if (sUnit === 'y' || sUnit === 'years') {
      return time.utcOffset(TzTw).set('month', 0).set('date', 1).format('YYYY-MM-DD')
    }
    // if days
    return time.utcOffset(TzTw).format('YYYY-MM-DD')
  }

  return time.utcOffset(TzTw).subtract(+nNum, sUnit).format('YYYY-MM-DD')
}

/**
 * 傳回今天開始往前移動 n 個時間單位後零點的 linux 時間的毫秒數
 * 內定傳回本日零點的 linux 時間的毫秒數
 * e.q.  '3d' 等於三天前
 *
 * @param {String} perid 單位時間量
 *
 * @returns {String} 傳回日期, 格式為 YYYY-MM-DD
 */
const age = (perid) => atBefore(perid)

// function about address
const Cities = [
  '台北市', '新北市', '基隆市', '宜蘭縣', '新竹市', '新竹縣',
  '桃園市', '苗栗縣', '台中市', '彰化縣', '南投縣', '嘉義市',
  '嘉義縣', '雲林縣', '台南市', '高雄市', '澎湖縣', '金門縣',
  '屏東縣', '台東縣', '花蓮縣', '連江縣'
]

const CityRE = /[\u{4e00}-\u{9fff}]{2}[縣市]$/u
const TownRE = /[\u{4e00}-\u{9fff}]+[鄉鎮市區]$/u

/**
 * 判斷是否為正確的台灣縣市名稱
 *
 * @param {String} city 縣市名稱
 * @returns {boolean} 是否為台灣縣市名稱
 */
const validCityName = (city) => CityRE.test(city) && (Cities.indexOf(city) > -1)

/**
 * 修正台灣縣市名稱, '臺' 或 '台' 則統一使用 '台'
 *
 * @param {String} city 縣市名稱
 * @returns {String} 修正後的台灣縣市名稱
 */
const fixCityName = (city) => (city || '').replace(/臺/, '台')
  .replace('台北縣', '新北市')
  .replace(/(桃園|台中|台南|高雄)縣/, '$1市')

/**
 * 判斷是否為鄉鎮市區名稱
 *
 * @param {String} town 鄉鎮市區
 * @returns {Boolean} 是否為鄉鎮市區
 */
const validTownName = (val) => TownRE.test(val)

// function about string
/**
 * 清除字串中所有的空白字元
 *
 * @param {String} s 來源字串
 * @returns {String} 傳回字串
 */
const clearSpace = (s) => (s || '').replace(/\s+/g, '')

module.exports = {
  jsonInline: jsonInline,
  validPeriod: validPeriod,
  thisYear: thisYear,
  thisMonth: thisMonth,
  thisWeek: thisWeek,
  thisDate: thisDate,
  atBefore: atBefore,
  age: age,
  validCityName: validCityName,
  validTownName: validTownName,
  fixCityName: fixCityName,
  clearSpace: clearSpace
}
